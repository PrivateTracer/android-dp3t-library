package org.privatetracer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import org.privatetracer.dp3t.CuckooFilter
import org.privatetracer.dp3t.Dp3t
import org.privatetracer.dp3t.EpochInfo

@ExperimentalUnsignedTypes
fun ByteArray.toHexString(): String {
    return this.asUByteArray().joinToString("") { it.toString(16).padStart(2, '0') }
}

class MainActivity : AppCompatActivity() {
    @ExperimentalUnsignedTypes
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Dp3t.init()

        val epochInfoNow = EpochInfo()
        val epochInfoZero = EpochInfo(0)

        Log.d("MainActivity", "Now id: ${epochInfoNow.id}, next_after: ${epochInfoNow.nextAfter}")
        Log.d("MainActivity", "Zero id: ${epochInfoZero.id}, next_after: ${epochInfoZero.nextAfter}")

        val seed = Dp3t.generateSeed()
        val ephid = Dp3t.generateEphId(seed)
        val hashid = Dp3t.generateHashedId(ephid, epochInfoNow.id)

        Log.d("MainActivity", "Seed: ${seed.toHexString()}")
        Log.d("MainActivity", "EphId: ${ephid.toHexString()}")
        Log.d("MainActivity", "HashId: ${hashid.toHexString()}")

        val filter = CuckooFilter()
        Log.d("MainActivity", "Cuckoofilter contains: ${filter.contains(hashid)}")
        Log.d("MainActivity", "Adding: ${hashid}")
        filter.add(hashid)
        Log.d("MainActivity", "Cuckoofilter contains: ${filter.contains(hashid)}")

    }
}
