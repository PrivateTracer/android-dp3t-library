mod java_glue;
pub use crate::java_glue::*;
use dp3t::cuckoo::SimpleCuckooFilter;
use dp3t::epoch::EpochInfo;
use dp3t::types::*;
pub use dp3t::*;

fn generate_eph_id(seed: &[u8]) -> Result<EphemeralId, String> {
    if seed.len() != EPHID_SEED_LENGTH {
        return Err(format!("Input seed is not of size {}", EPHID_SEED_LENGTH));
    }

    let mut s = [0u8; EPHID_SEED_LENGTH];
    s.copy_from_slice(&seed[0..EPHID_SEED_LENGTH]);

    Ok(generate_eph_ephid(&s))
}

fn generate_hid(id: &[u8], ts: Epoch) -> Result<EphemeralSeed, String> {
    if id.len() != EPHID_ID_LENGTH {
        return Err(format!("Input id is not of size {}", EPHID_ID_LENGTH));
    }

    let mut s = [0u8; EPHID_ID_LENGTH];
    s.copy_from_slice(&id[0..EPHID_ID_LENGTH]);

    Ok(generate_hashed_identity(&s, &ts))
}

/// Helper method to return struct field
fn get_id(_this: &EpochInfo) -> Epoch {
    _this.id
}

/// Helper method to return struct field
fn get_next_after(_this: &EpochInfo) -> i64 {
    _this.next_after
}

fn hashed_entry_helper(te: &[u8]) -> Result<HashedIdentity, String> {
    if te.len() != EPHID_HASH_LENGTH {
        return Err(format!("Input is not of size {}", EPHID_HASH_LENGTH));
    }

    let mut s = [0u8; EPHID_HASH_LENGTH];
    s.copy_from_slice(&te[0..EPHID_HASH_LENGTH]);
    Ok(s)
}

// Helper method for adding things to the cuckoo filter
fn add_helper(_this: &mut SimpleCuckooFilter, x: &[u8]) -> Result<(), String> {
    let r = hashed_entry_helper(x);
    if r.is_err() {
        return r.map(|_| ());
    }
    _this.add(&r.unwrap()).map_err(|x| x.to_string())
}

// Helper method for looking up things in the cuckoo filter
fn contains_helper(_this: &mut SimpleCuckooFilter, x: &[u8]) -> bool {
    let r = hashed_entry_helper(x);
    if r.is_err() {
        return false;
    }
    _this.contains(&r.unwrap())
}
