use crate::epoch::*;
use crate::*;
use dp3t::cuckoo::SimpleCuckooFilter;
use jni_sys::*;

foreign_class!(class Dp3t {
    /// Generates a 32 byte seed that can be used to generate ephemeral ids
    /// This seed needs to be used locally to generate ids for broadcasting
    /// and the seed needs to be uploaded to the server when the user is marked
    /// as infected
    fn generate_eph_seed() -> [u8; 32]; alias generateSeed;
    /// Generates an ephemeral id that can be used for broadcasting
    ///
    /// # Arguments
    ///
    /// @param seed - A 32 byte array containing the seed
    fn generate_eph_id(seed: &[u8]) -> Result<[u8; 16], String>; alias generateEphId;
    /// Generates a hashed id for local storage
    ///
    /// # Arguments
    ///
    /// @param id - A 16 byte array containing the ephemeral id
    /// @param ts - A signed 32 bit integer containing the epoch
    fn generate_hid(id: &[u8], ts: i32) -> Result<[u8; 32], String>; alias generateHashedId;

    foreign_code r#"
    /**
     * Initializes the native library, call this before you do anything else!
     */
    public static void init() { 
        System.loadLibrary("dp3t");
    }
"#;
});

foreign_class!(class EpochInfo {
    self_type EpochInfo;
    /// Creates a new EpochInfo that is created using the current unix timestamp.
    constructor EpochInfo::new() -> EpochInfo;
    /// Creates a new EpochInfo object from the specified unix timestamp.
    /// @param ts - A unix timestamp
    constructor EpochInfo::from_timestamp(ts: i64) -> EpochInfo;

    /// Gets the current Epoch Id
    fn get_id(&self) -> i32; alias getId;
    /// Gets the unix timestamp after which the app should request a new Epoch Id
    /// In order to request a new EpochId, simply create a new EpochInfo. Do not call
    /// getId on this again as it will not have changed.
    fn get_next_after(&self) -> i64; alias getNextAfter;
});

foreign_class!(class CuckooFilter {
    self_type SimpleCuckooFilter;
    /// Creates a new SimpleCuckooFilter with default capacity
    constructor SimpleCuckooFilter::new() -> SimpleCuckooFilter;

    /// Creates a new SimpleCuckooFilter with specified capacity
    ///
    /// # Arguments
    ///
    /// @param cap - The strict maximum of items this filter can ever contain
    constructor SimpleCuckooFilter::with_capacity(cap: usize) -> SimpleCuckooFilter;

    /// Serialize the current SimpleCuckooFilter. Can fail
    fn SimpleCuckooFilter::serialize(&self) -> Result<String, String>;

    /// Serialize the current SimpleCuckooFilter as a blob. Can fail
    fn SimpleCuckooFilter::serialize_blob(&self) -> Result<String, String>;

    /// Try to create a SimpleCuckooFilter from json representation. Can fail
    ///
    /// # Arguments
    ///
    /// @param json_string - A string that contains a valid JSON representation of a cuckoo filter
    fn SimpleCuckooFilter::deserialize(json_string: String) -> Result<SimpleCuckooFilter, String> {
        SimpleCuckooFilter::deserialize(&json_string)
    }

    /// Try to create a SimpleCuckooFilter from blob representation. Can fail
    ///
    /// # Arguments
    ///
    /// @param blob_string - A string that contains a valid blob representation of a cuckoo filter
    fn SimpleCuckooFilter::deserialize_blob(blob_string: String) -> Result<SimpleCuckooFilter, String> {
        SimpleCuckooFilter::deserialize_blob(&blob_string)
    }

    /// Add an entry to the SimpleCuckooFilter.
    ///
    /// # Arguments
    ///
    /// @param x - A correctly sized byte array to enter into the filter
    /// representation of a cuckoo filter
    ///
    /// @exception Exception - An exception is thrown when you try to
    /// add things that do not have the right size. An exception is
    /// also throw if the filter is nearing max capacity, in which
    /// case an existing entry has been forcefully removed to make
    /// room for your entry!
    fn add_helper(&mut self, x : &[u8]) -> Result<(), String>; alias add;

    /// Checks whether a particular byte array might be present in the cuckoo filter
    ///
    /// # Arguments
    ///
    /// @param x - The byte array to check for
    fn contains_helper(&mut self, x : &[u8]) -> bool; alias contains;
});
