mod swig_foreign_types_map {}
use std::mem;

// TODO: Use the macros?

impl SwigDeref for JavaByteArray {
    type Target = [u8];
    fn swig_deref(&self) -> &Self::Target {
        let i8slice = self.to_slice();
        unsafe { std::slice::from_raw_parts(i8slice.as_ptr() as *const u8, i8slice.len()) }
    }
}

impl SwigInto<jbyteArray> for [u8; 32] {
    fn swig_into(self, env: *mut JNIEnv) -> jbyteArray {
        let b = unsafe { mem::transmute::<[u8; 32], [i8; 32]>(self) };
        JavaByteArray::from_slice_to_raw(&b, env)
    }
}

impl SwigFrom<jbyteArray> for JavaByteArray {
    fn swig_from(x: jbyteArray, env: *mut JNIEnv) -> Self {
        JavaByteArray::new(env, x)
    }
}

impl SwigInto<jbyteArray> for [u8; 16] {
    fn swig_into(self, env: *mut JNIEnv) -> jbyteArray {
        let b = unsafe { mem::transmute::<[u8; 16], [i8; 16]>(self) };
        JavaByteArray::from_slice_to_raw(&b, env)
    }
}
