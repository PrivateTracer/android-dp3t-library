use rust_swig::{JavaConfig, LanguageConfig};
use std::{env, fs, path::Path};

fn main() {
    env_logger::init();
    let out_dir = env::var("OUT_DIR").unwrap();
    let in_src = Path::new("src").join("java_glue.in.rs");
    let out_src = Path::new(&out_dir).join("java_glue.rs");

    let src_path = Path::new(".")
        .join("..")
        .join("..")
        .join("..")
        .join("src-gen")
        .join("main")
        .join("java")
        .join("org")
        .join("privatetracer")
        .join("dp3t");

    fs::create_dir_all(&src_path).expect("Failed to create path");

    let swig_gen = rust_swig::Generator::new(LanguageConfig::JavaConfig(
        JavaConfig::new(src_path, "org.privatetracer.dp3t".into())
            .use_null_annotation_from_package("androidx.annotation".into())
            .use_optional_package("java8.util".into()),
    ))
    .rustfmt_bindings(true)
    .merge_type_map("jni_support", include_str!("src/jni-include.rs"));

    swig_gen.expand("android bindings", &in_src, &out_src);

    println!("cargo:rerun-if-changed={}", in_src.display());
    println!("cargo:rerun-if-changed=src/jni-include.rs");
}
