# Android DP-3T Library

This is an Android DP-3T library that's using the [Rust implementation](https://gitlab.com/PrivateTracer/dp3t-rust) and wraps them in Java.

[Latest build artifacts](https://gitlab.com/PrivateTracer/android-dp3t-library/-/jobs/artifacts/master/download?job=build)

The output that you can use in another project is the `.aar` file that's generated in the following path:

```
dp3t/build/outputs/aar/dp3t-debug.aar
dp3t/build/outputs/aar/dp3t-release.aar
```

You can copy these into your own project.

## Development

In order to develop or build this project you need to have the Android NDK installed (version 21.0.6113669)
as well as [rust](https://www.rust-lang.org/tools/install).

The following rust tool chains need to be installed:

- aarch64-linux-android
- arm-linux-androideabi
- i686-linux-android
- x86_64-linux-android

You can install these using the following command:

```
$ rustup target add armv7-linux-androideabi
$ rustup target add aarch64-linux-android
$ rustup target add x86_64-linux-android
$ rustup target add i686-linux-android
```

Last but not least, rust needs to be able to find the Android linkers. These are not on your PATH by default
and you have to add them to your PATH manually. The location of the linkers should look like this:

```
export PATH=$PATH:${ANDROID_SDK}/ndk/<NDK_VERSION>/toolchains/llvm/prebuilt/<HOST_PLATFORM>/bin
```

_Note:_ The PATH needs to be available on your login shell, you can add it to `.bash_profile`.
_Note:_ On Windows, open Environment Variables and add to PATH there.

## Usage

Before doing anything you need to initialize the library (so it loads the native library) by calling the
following during startup:

```kotlin
// Put in Application onCreate
org.privatetracer.dp3t.Dp3t.init()
```

## Broadcasting Ephemeral Ids

The bluetooth advertisement has to send out the same hashed identity during an entire epoch. In order to do
this some things need to be done first and stored locally as well.

### Step 1: Epoch

Before we can do anything we need to know which Epoch we're in you can do this by constructing a new `EpochInfo`
object as follows.

```kotlin
val epoch = EpochInfo()
```

This object contains 2 fields namely:

- `id` - The id of the current epoch
- `next_after` - Unix timestamp after which you should construct a new `EpochInfo` object, to get a new id

### Step 2: Generate a seed

Whenever you enter a new epoch, you should generate a new seed as well. You can do so like this:

```kotlin
val seed = Dp3t.generateSeed()
```

**Important:**
You now have a `seed` and an `epoch.id`, you have to store these together locally so you can send these to the
server in case the user of the application is marked as infected.

### Step 3: Generate Ephemeral Id

Now it is time to generate an ephemeral id that can be used in the hashed identity.

```kotlin
val ephId = Dp3t.generateEphId(seed)
```

**Important:** This needs to be broadcasted.

This is a 16 byte array that can fit in the protocol.

## Receiving Ephemeral Ids

Life is about give and take. So we also have to be ready to receive foreign ephemeral ids.

### Step 1: Epoch

You need to generate the epoch to know later which ids are infected.

```kotlin
val epoch = EpochInfo()
```

This object contains 2 fields namely:

- `id` - The id of the current epoch
- `next_after` - Unix timestamp after which you should construct a new `EpochInfo` object, to get a new id

### Step 2: Generate a hashed identity

Whenever we receive a remote ephemeral id we need to convert it into a hashed identity.

```kotlin
val hashedId = Dp3t.generateHashedId(remoteEphId, epoch.id)
```

**Important:** When receiving this hashed identity from another device, you need to store it locally.

The returned value is a 32 byte array.

## Working with CuckooFilters

Working with CuckooFilter on the client side should mostly consist of the following:
```kotlin
val filter = CuckooFilter()
val entry = byteArrayOf(0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38,
        0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38, 0x2E, 0x38)
filter.contains(entry) // == false
filter.add(entry)
filter.contains(entry) // == true, probably :P
```

Normally you should not construct your own filters or call the `add`
method at all. Instead, use `CuckooFilter.deserialize` to create a
filter based on the data you (will) receive from the server.

# Troubleshooting

## Generate functions throw Exceptions

If any of the generate functions (with parameters) throw exceptions, this means that the array you
passed in is of the incorrect size. The exception itself contains information on what went wrong.

## Help I need the NDK

Unfortunately for now the generated AAR file isn't fully standalone. So any project using this
will still need the NDK. You just don't need to have rust installed which should make your life easier.
